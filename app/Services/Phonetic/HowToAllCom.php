<?php


namespace App\Services\Phonetic;


use App\Models\Cache;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use PHPHtmlParser\Dom;

class HowToAllCom
{
    const ADDR = 'https://how-to-all.com/%D0%BA%D0%B0%D1%80%D1%82%D0%BE%D1%87%D0%BA%D0%B0:';

    public function handle($word)
    {
        $cache = Cache::find(self::class, $word->id);
        if ($cache) {
            return $cache->data;
        } else {
            return $this->handleRemote($word);
        }
    }

    private function handleRemote($word)
    {
        $content = [];

        $response = (new Client)->request('GET', self::ADDR . ($word->word), [
            'allow_redirects' => true,
        ]);

        $contents = $response->getBody()->getContents();
        $dom = (new Dom())->loadStr($contents);
        foreach ($dom->find('a') as &$link) {
            $link->setAttribute('href', '/' . $link->text(true));
        }

        foreach ($dom->find('.adynamic > div') as $part) {
            $p = ['content' => []];
            $p['title'] = $part->find('h2')->text;
            if ($part->find('.chr')->count() == 0) {
                if ($part->find('#dict160')->count() > 0) {
                    foreach ($parts = $part->find('#dict160 p') as $item) {
                        $item = $item->innerHTML;
                        preg_match('/<a?.+>(.+?)<\/a>/', $item, $matches);
                        $p['content'][] = preg_replace('/href=\'.+?\'/', 'href="/' . ($matches[1] ?? '') . '"', $item);
                    }
                } else {
                    try {
                        $p['content'][] = $part->find('.dif, .gr-data-self')->innerHTML;
                    } catch (\Exception $exception) {

                    }
                }
            } else {
                foreach ($part->find('.chr') as $item) {
                    if ($item->find('strong')->count() > 0)
                        $p['content'][] = ['title' => $item->find('strong')->text(true), 'value' => $item->find('.chrd')->text(true)];
                }
            }
            $content[] = $p;
        }

        $content = $this->prepare($content);

        $cache = Cache::create([
            'item_class' => self::class,
            'item_id' => $word->id,
            'data' => $content,
        ]);

        return $content;
    }

    private function prepare($data)
    {
        $clean = [];
        foreach ($data as $elem) {
            if (Str::contains($elem['title'], 'Морфологический разбор')) {
                $clean['morph'] = $this->prepareMorph($elem);
            } else if (Str::contains($elem['title'], 'Фонетический разбор')) {
                $clean['phon'] = $this->preparePhon($elem);
            } else if (Str::contains($elem['title'], 'Что означает')) {
                $clean['meaning'] = $this->prepareMeaning($elem);
            } else if (Str::contains($elem['title'], 'Разбор по составу')) {
                $clean['sost'] = $this->prepareSost($elem);
            } else if (Str::contains($elem['title'], 'Синонимы')) {
                $clean['sinon'] = $this->prepareSinon($elem);
            }
        }
        return $clean;
    }

    private function prepareMorph($elem)
    {
        $val = ['title' => $elem['title'], 'content' => []];
        foreach ($elem['content'] as $value) {
            $el = [
                'title' => $value['title'],
                'value' => []
            ];
            foreach ($exploded = explode(';', $value['value']) as $i) {
                if (count($exploded) > 1) {
                    $j = explode(':', $i);
                    if (isset($j[1]))
                        $el['value'][trim($j[0])] = trim($j[1] ?? '');
                } else
                    $el['value'][] = trim($i);
            }
            $val['content'][] = $el;
        }
        return $val;
    }

    private function preparePhon($elem)
    {
        return $elem;
    }

    private function prepareMeaning($elem)
    {
        return $elem;
    }

    private function prepareSost($elem)
    {
        return $elem;
    }

    private function prepareSinon($elem)
    {
        $words = [];
        $dom = (new Dom())->loadStr($elem['content'][0]);
        foreach ($dom->find('a') as $link) {
            $words[] = $link->text;
        }
        return [
            'title' => $elem['title'],
            'content' => $words,
        ];
    }
}
