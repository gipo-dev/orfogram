<?php


namespace App\Services\Phonetic;


use App\Models\Word;

interface IPhoneticService
{
    /**
     * @param Word $word
     * @param $accent
     * @return PhoneticWord
     */
    public function handle(Word $word, $accent);
}
