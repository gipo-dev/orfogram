<?php


namespace App\Services\Phonetic;


use App\Models\Linkable;

class PhoneticWord
{

    /**
     * @var string[]
     */
    public $related;

    /**
     * @var string
     */
    public $data;
}
