<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class SpecialWord extends Model
{
    use AsSource;
    use Filterable;
    use HasMeta;

    /**
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'word',
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'word',
    ];

    public $timestamps = false;

    protected $guarded = [];

    protected $attributes = ['data' => ''];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->meta_patterns = [
            'title' => 'Проверочное слово "{key}" онлайн',
            'description' => 'Проверочное слово "{key}" онлайн',
            'h1' => '"{key}" - проверочное слово',
        ];
    }
}
