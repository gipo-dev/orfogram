<?php


namespace App\Models;


class PopularWords
{
    const BY_VIEWS = 'views';
    const BY_DATE = 'created_at';

    public static function get($by = self::BY_VIEWS, $result = true)
    {
        $popular = Option::key('popular_' . $by);
        if ($popular->updated_at > now()->subHours(12) && trim($popular->value) != '') {

        } else {
            PopularWords::generate($popular, $by);
            $popular = Option::find($popular->id);
        }
        return Word::whereIn('id', explode(',', $popular->value))->get();
    }

    public static function generate($option, $by = self::BY_VIEWS)
    {
        $option->update([
            'value' => implode(',', Word::select('id')
                ->orderByDesc($by)
                ->limit(20)
                ->get()->pluck('id')->toArray()),
            'updated_at' => now(),
        ]);
    }
}
