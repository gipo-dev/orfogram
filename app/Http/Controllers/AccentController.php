<?php

namespace App\Http\Controllers;

use App\Models\Accent;
use Illuminate\Http\Request;

class AccentController extends Controller
{
    public function list()
    {
        $words = Accent::orderBy('title')->simplePaginate(20);
        \Meta::set('menu', 3);
        return view('accent_list', compact('words'));
    }

    public function show(Request $request, Accent $word)
    {
        \Meta::set('title', $word->title . " - куда поставить ударение в слове");
        \Meta::set('menu', 3);
        return view('accent', compact('word'));
    }
}
