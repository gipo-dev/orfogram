<?php

namespace App\Console\Commands;

use App\Models\Word;
use Illuminate\Console\Command;

class WordGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'words:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $words = collect(explode(PHP_EOL, file_get_contents(storage_path('links.txt'))));
        $exists = Word::select(['id', 'word'])->get()->pluck('word');
        $words = collect($words->diff($exists)->all());
//        dd($words);
        foreach ($words->chunk(30) as $chunk) {
//            $insert = [];
            foreach ($chunk as $item) {
//                $insert[] = [
//                    'word' => $item,
//                    'views' => 0,
//                ];
                try {
                    Word::insert(['word' => $item]);
                } catch (\Exception $exception) {
                }
            }
//            Word::insert($insert);
        }
    }
}
