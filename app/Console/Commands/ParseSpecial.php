<?php

namespace App\Console\Commands;

use App\Models\SpecialWord;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ParseSpecial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:special';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $_rows = [];
        if (($handle = fopen(storage_path('article_all_1.csv'), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 100000, ";")) !== FALSE) {
                $num = count($data);
                $line = [];
                for ($c = 0; $c < $num; $c++) {
                    $line[] = $data[$c];
                }
                $_rows[] = $line;
            }
            fclose($handle);
        }
        foreach ($_rows as $i => $row) {
            if ($i == 0)
                continue;

            try {
                $word = mb_strtolower(explode(' проверочное слово', $row[1])[0]);
                SpecialWord::create([
                    'word' => $word,
                    'slug' => Str::slug($word),
                    'data' => $row[2],
                ]);
            } catch (\Exception $exception) {
            }
        }
    }
}
