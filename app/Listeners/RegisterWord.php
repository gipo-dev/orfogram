<?php

namespace App\Listeners;

use App\Events\WordSearch;
use App\Models\Word;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RegisterWord
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param WordSearch $event
     * @return void
     */
    public function handle(WordSearch $event)
    {
        $exists = Word::firstOrCreate([
            'word' => $event->word,
        ], [
            'views' => 1,
        ]);
        if (!$exists->wasRecentlyCreated) {
            $exists->update([
                'views' => $exists->views + 1,
            ]);
        }
    }
}
