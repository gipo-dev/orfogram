<?php

namespace App\Orchid\Screens\SpecialWord;

use App\Models\SpecialWord;
use App\Orchid\Filters\WordFilter;
use App\Orchid\Layouts\SpecialWord\SpecialWordListLayout;
use App\Orchid\Layouts\Word\WordFilterLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Color;

class SpecialWordListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Проверочные слова';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'words' => SpecialWord::filters()
                ->filtersApply([WordFilter::class])
                ->defaultSort('word')
                ->paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Создать')
                ->icon('plus')
                ->type(Color::SUCCESS())
                ->route('platform.specials.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            WordFilterLayout::class,
            SpecialWordListLayout::class,
        ];
    }
}
