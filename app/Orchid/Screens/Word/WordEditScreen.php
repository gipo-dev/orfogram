<?php

namespace App\Orchid\Screens\Word;

use App\Http\Requests\Admin\WordRequest;
use App\Models\Cache;
use App\Models\Word;
use App\Orchid\Layouts\Word\WordEditMeta;
use App\Orchid\Layouts\Word\WordEditPhonetic;
use App\Orchid\Layouts\Word\WordEditRows;
use App\Services\Phonetic\HowToAllCom;
use App\Services\Phonetic\PhoneticOnlineRu;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Alert;

class WordEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование';

    /**
     * Query data.
     *
     * @param Word $word
     * @return array
     */
    public function query(Word $word): array
    {
        $data = [
            'word' => $word,
            'phonetic' => ($word->phonetic()['phonetic']->data ?? ''),
        ];

        $this->getPhonetic($data, $word);

        return $data;
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Удалить')
                ->type(Color::DANGER())->icon('trash')
                ->confirm('Вы действительно желаете удалить данный ключ?')
                ->method('delete'),
            Link::make('Перейти')
                ->type(Color::INFO())->icon('eye')
                ->route('open', ['type' => 'word', 'id' => \request()->route()->parameter('word')])
                ->target('_blank'),
            Button::make('Сохранить')
                ->type(Color::SUCCESS())->icon('paper-plane')
                ->method('update'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            WordEditRows::class,
            WordEditPhonetic::class,
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(WordRequest $request)
    {
        $word = Word::find($request->word['id']);
        $word->update($request->word);

        if ($cache = Cache::find(PhoneticOnlineRu::class, $word->id) ?? false) {
            $data = $cache->data;
            $data->data = $request->phonetic;
            $cache->update(['data' => $data]);
        };

        if ($cache = Cache::find(HowToAllCom::class, $word->id) ?? false) {
            $data = $cache->data;
            foreach ($request['additional'] as $k => $item) {
                $data[$k] = $item;
            }
            $cache->update(['data' => $data]);
        };

        Alert::success('Изменения успешно сохранены');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function open(Request $request)
    {
        return redirect(route('word', $request->word['word']));
    }

    /**
     * @param Request $request
     * @param Word $word
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $word = Word::find($request->word['id']);
        $word->delete();
        return redirect(route('platform.word.list'));
    }

    /**
     * @param $data
     * @param $word
     */
    private function getPhonetic(&$data, &$word)
    {
        if ($word->phonetic()['additional'] ?? false) {
            foreach ($word->phonetic()['additional'] as $key => $val) {
                if (in_array($key, ['sost', 'sinon']))
                    continue;
                if (is_array($val)) {
                    $lines = '<p>';
                    $rows = [];
                    foreach ($val['content'] as $line) {
                        if (isset($line['value'])) {
                            try {
                                if (!is_array($line['value']))
                                    $line['value'] = [$line['value']];
                                $rows[] = '<strong>' . $line['title'] . '</strong>' . implode(', ', $line['value'] ?? []);
                            } catch (\Exception $exception) {
                                dd($line, $exception->getMessage());
                            }
                        } else {
                            $lines .= PHP_EOL . $line;
                        }
                    }
                    $lines .= implode('</p><p>', $rows) . '</p>';
                } else {
                    $lines = $val;
                }
                $data['additional'][$key] = $lines;
            }
        }
    }
}
