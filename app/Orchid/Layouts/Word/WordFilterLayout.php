<?php

namespace App\Orchid\Layouts\Word;

use App\Orchid\Filters\WordFilter;
use Orchid\Filters\Filter;
use Orchid\Screen\Layouts\Selection;

class WordFilterLayout extends Selection
{
    public $template = self::TEMPLATE_LINE;

    /**
     * @return Filter[]
     */
    public function filters(): array
    {
        return [
            WordFilter::class,
        ];
    }
}
