<?php

namespace App\Orchid\Layouts\Word;

use Leshkens\OrchidTinyMCEField\TinyMCE;
use Orchid\Screen\Field;
use Orchid\Screen\Layouts\Rows;

class WordCreatePhonetic extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Разбор слова';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $titles = [
            'morph' => 'Морфологический разбор',
            'phon' => 'Звуко-буквенный разбор',
            'meaning' => 'Значение слова',
        ];
        $rows = [
            TinyMCE::make('phonetic')
                ->theme('modern')
                ->config(['content_css' => [
                    '/css/ui.css',
                    '/phonetic/css/ui.css',
                ]])
                ->title('Фонетический разбор'),
        ];

        foreach (['morph' => '', 'phon' => '', 'meaning' => ''] as $k => $item) {
            $rows[] = TinyMCE::make('additional.' . $k)
                ->theme('modern')
                ->config(['content_css' => [
                    '/css/ui.css',
                    '/phonetic/css/ui.css',
                ]])
                ->title($titles[$k] ?? '');
        }
        return $rows;
    }
}
