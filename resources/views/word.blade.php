@extends('partials.body')

@section('content')
{{--    <h1 class="mt-4">Фонетический разбор слова - "{{ mb_ucfirst($word) }}" (звуки и буквы)</h1>--}}
    <h1 class="mt-4">{{ $_word->h1 }}</h1>
    @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
        <a href="{{ route('platform.word.edit', $_word) }}" class="btn btn-primary">Редактировать</a>
    @endif

    @if($phonetic['phonetic'])
        {!! $phonetic['phonetic']->data !!}
    @endif
    @if($phonetic['additional'])
        <div class="mt-4">
            @if($morph = $phonetic['additional']['morph']['content'] ?? $phonetic['additional']['morph'] ?? false)
                <div class="card">
                    <h2 class="card-header">Морфологический разбор слова "{{ $word }}"</h2>
                    <div class="card-body">
                        @if(is_array($morph))
                            @foreach($morph as $v)
                                <p>
                                    <strong>{{ $v['title'] }}</strong>
                                    @if(count($v['value']) > 1)
                                        @foreach($v['value'] as $k => $j)
                                            <span class="text-secondary">{{ $k }}: </span>{{ $j }},
                                        @endforeach
                                    @else
                                        {{ $v['value'][0] ?? '' }}
                                    @endif
                                </p>
                            @endforeach
                        @else
                            {!! $morph !!}
                        @endif
                    </div>
                </div>
            @endif

            @if($phon = $phonetic['additional']['phon']['content'] ?? $phonetic['additional']['phon'] ?? false)
                <div class="card">
                    <h2 class="card-header">Звуко-буквенный разбор слова "{{ $word }}"</h2>
                    <div class="card-body">
                        @if(is_array($phon))
                            @foreach($phon as $v)
                                <p>
                                    <strong>{{ $v['title'] }}</strong>
                                    {{ $v['value'] ?? '' }}
                                </p>
                            @endforeach
                        @else
                            {!! $phon !!}
                        @endif
                    </div>
                </div>
            @endif

            @if($meaning = $phonetic['additional']['meaning']['content'] ?? $phonetic['additional']['meaning'] ?? false)
                <div class="card">
                    <h2 class="card-header">Значение слова "{{ $word }}"</h2>
                    <div class="card-body">
                        @if(is_array($meaning))
                            @foreach($meaning as $v)
                                <p>
                                    {!! $v ?? '' !!}
                                </p>
                            @endforeach
                        @else
                            {!! $meaning !!}
                        @endif
                    </div>
                </div>
            @endif

            @if($sost = $phonetic['additional']['sost']['content'] ?? false)
                <div class="card">
                    <h2 class="card-header">Разбор по составу (морфемный) "{{ $word }}"</h2>
                    <div class="card-body sostav">
                        <a href="{{ route('composition', $word) }}">
                            @foreach($sost as $v)
                                <p>
                                    {!! $v ?? '' !!}
                                </p>
                            @endforeach
                        </a>
                    </div>
                </div>
            @endif

            @if($sinon = $phonetic['additional']['sinon']['content'] ?? false)
                <div class="card">
                    <h2 class="card-header">Синонимы к слову "{{ $word }}"</h2>
                    <div class="card-body sostav">
                        @foreach($sinon as $sin)
                            <a href="/phonetic/{{ $sin }}" class="d-ib px-1 py-2">{{ $sin }}</a>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    @endif

    <h4 class="mt-4">Примеры других слов с разборами:</h4>
    <ul class="list related">
        @if($phonetic['phonetic'] && isset($phonetic['phonetic']->related))
            @foreach($phonetic['phonetic']->related as $word)
                <li>
                    <a href="{{ route('word', $word) }}">{{ $word }}</a>
                </li>
            @endforeach
        @endif
        @foreach($_word->getRelated() as $word)
            <li>
                <a href="{{ route('word', $word->word) }}">{!! $word->word !!}</a>
            </li>
        @endforeach
    </ul>
@endsection
