@extends('partials.body')

@section('content')
    <div class="alert alert-danger mt-3">
        <h4 class="mt-0">Ошибка при разборе слова</h4>
        Мы делаем разбор только русских слов. Не допускаются в словах иностранные буквы, знаки пунктуации, цифры и
        другие символы, не являющиеся русскими буквами. С учётом этого попробуйте ещё раз поискать слово или
        воспользуйтесь алфавитным указателем.
    </div>

    <h3 class="mt-4 mb-3">Попробуйте разобрать другое слово</h3>
    <form action="{{ route('search') }}" method="post">
        @csrf
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Введите слово" aria-label="Recipient's username"
                   aria-describedby="button-addon2" name="q" required>
            <button class="btn btn-success" type="submit" id="button-addon2">Разобрать</button>
        </div>
    </form>
@endsection
