@extends('partials.body')

@section('content')
    <h1 class="mt-4">Фонетический разбор слова онлайн</h1>
    <table class="table table-striped table-borderless border-bottom-0 mt-4 mb-4">
        @foreach($words as $word)
            <tr>
                <td>
                    <a href="{{ route('word', $word->word) }}">{{ urldecode($word->word) }}</a>
                </td>
            </tr>
        @endforeach
    </table>
    {{ $words->links() }}
@endsection
