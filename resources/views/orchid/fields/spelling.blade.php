@if(isset($variants) && $variants && is_array($variants))
    <div class="mb-3">
        @foreach($variants as $i => $variant)
            <div class="variant variant-{{ $i == 0 ? 'ok' : 'error' }} mr-2">
                <span class="title-small">{{ $i == 0 ? 'Правильно' : 'Не правильно' }}</span>
                <span class="body">{!! $variant !!}</span>
            </div>
        @endforeach
    </div>
@endif
