@extends('partials.body')

@section('content')
    <div class="card mt-4">
        <h1 class="card-header">{!! $word->h1 !!}</h1>
        <div class="card-body">
            @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
                <div>
                    <a href="{{ route('platform.spelling.edit', $word->id) }}" class="btn btn-primary btn-sm mb-2">Редактировать</a>
                </div>
            @endif
            {!! \App\Services\Helper::close_tags($word->determination) !!}
        </div>
    </div>

    @if(($variants = $word->getVariantsArray()) && is_array($variants))
        <div class="d-flex justify-content-center flex-wrap spelling-variants">
            @foreach($variants as $i => $variant)
                <div class="variant variant-{{ $i == 0 ? 'ok' : 'error' }} shadow-lg">
                    <span class="title-small">{{ $i == 0 ? 'Правильно' : 'Не правильно' }}</span>
                    <span class="body">{!! $variant !!}</span>
                </div>
            @endforeach
        </div>
    @endif

    <div class="card">
        <h2 class="card-header">Правописание {!! $word->title !!}</h2>
        <div class="card-body">
            {!! \App\Services\Helper::close_tags($word->spelling) !!}
        </div>
    </div>

    <div class="card mb-4">
        <h2 class="card-header">Примеры правильного написания {!! $word->title !!}</h2>
        <div class="card-body pb-4">
            <ul class="mb-2">{!! \App\Services\Helper::close_tags($word->example) !!}</ul>
        </div>
    </div>

    <ul class="list related">
        @foreach($word->getRelated() as $word)
            <li>
                <a href="{{ route('spelling', $word->slug) }}">{!! $word->title !!}</a>
            </li>
        @endforeach
    </ul>

@endsection
