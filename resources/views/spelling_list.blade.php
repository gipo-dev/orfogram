@extends('partials.body')

@section('content')
    <h1 class="mt-4">Как правильно пишутся слова онлайн</h1>
    <table class="table table-striped table-borderless border-bottom-0 mt-4 mb-4">
        @foreach($words as $word)
            <tr>
                <td>
                    <a href="{{ route('spelling', $word->slug) }}">{!! $word->title ?? '' !!}</a>
                </td>
            </tr>
        @endforeach
    </table>
    {{ $words->links() }}
@endsection
