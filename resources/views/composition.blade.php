@extends('partials.body')

@section('content')
    <h1 class="mt-4">Разбор слова "{{ $word->word }}" по составу</h1>
    
    
    <form id="search-composition" data-url="{{ route('composition', 'kkk') }}" class="input-group mb-4" style="max-width: 500px">
        <input type="text" id="composition-word" class="form-control" value="{{ $word->word ?? '' }}" placeholder="Разобрать другое слово">
        <button class="btn btn-success" type="submit" id="button-addon2">Разобрать</button>
    </form>
</form>

@push('scripts')
<script>
    $(function() {
        $('#search-composition').submit(function (e) {
            e.preventDefault();
            var url = $(this).data('url').replace('kkk', '');
            window.location.href = url + $('#composition-word').val();
        });
    });
</script>
@endpush

    @if($phonetic ?? false)
        <div class="page-sostav">
            @foreach($phonetic['sost']['content'] as $v)
                <p>
                    {!! $v ?? '' !!}
                </p>
            @endforeach
        </div>
    @endif

    <a href="{{ route('word', $word->word) }}" class="btn btn-link mt-3 text-decoration-none"><span style="position:relative;top: -3px; text-decoration: none;">←</span> <span class="text-decoration-underline">Фонетический разбор слова {{ $word->word }}</span></a>

    <h4 class="mt-5">Примеры других слов с разборами:</h4>
    <ul class="list related">
        @foreach($word->getRelated() as $w)
            <li>
                <a href="{{ route('composition', $w->word) }}">{!! $w->word !!}</a>
            </li>
        @endforeach
    </ul>
@endsection
