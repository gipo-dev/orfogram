<html lang="en"><link type="text/css" rel="stylesheet" id="dark-mode-general-link" href="chrome-extension://dmghijelimhndkbmpgbldicpogfkceaj/data/content_script/general/dark_40.css"><link type="text/css" rel="stylesheet" id="dark-mode-custom-link"><style lang="en" type="text/css" id="dark-mode-custom-style"></style><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{!! \Meta::get('description') !!}">
    <meta name="author" content="">
    <link rel="icon" href="https://literaguru.ru/wp-content/uploads/2016/09/cropped-favicon-1-150x150.png" type="image/x-icon" />
    <link rel="shortcut icon" href="https://literaguru.ru/wp-content/uploads/2016/09/cropped-favicon-1-150x150.png" type="image/x-icon" />

    <title>{!! \Meta::get('title') !!}</title>

     <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(39463835, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/39463835" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

    <!-- Bootstrap core CSS -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    
    <!-- Yandex.RTB -->
<script>window.yaContextCb=window.yaContextCb||[]</script>
<script src="https://yandex.ru/ads/system/context.js" async></script>


<!-- Yandex.RTB R-A-749928-6 -->
<script>window.yaContextCb.push(()=>{
  Ya.Context.AdvManager.render({
    type: 'floorAd',
    blockId: 'R-A-749928-6'
  })
})</script>
</head>
<body>
